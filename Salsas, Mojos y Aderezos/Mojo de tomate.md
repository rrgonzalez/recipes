Mojo de Tomate
==============

Ingredientes:
-------------
+ 210 g. de tomate pelado y picado sin semillas
+ 45 g. de jugo de limón o naranja agria
+ 8 g. de ajo picado fino
+ 60 g. de cebolla picada fina
+ 5 g de sal.

Preparación:
------------
Añadir el jugo de limón, la cebolla, el ajo y la sal al tomate maduro. Este mojo es especial para huevos fritos o cocidos, pescado grillé o frito y tamal en hojas.