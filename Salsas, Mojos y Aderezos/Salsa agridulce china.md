Salsa agridulce china
=====================

Ingredientes:
-------------
+ 3/4 de taza de azúcar
+ 1/3 de taza de vinagre
+ 2/3 de taza de agua
+ 1/4 de taza de salsa de soja
+ 1 cucharada de ketchup
+ 2 cucharadas de almidón de maíz

Preparación:
------------
Coloca el azúcar, el vinagre, el agua, la salsa de soja, el ketchup y el almidón de maíz en el fuego y revuelve constantemente para evitar grumos, hasta que hierva.